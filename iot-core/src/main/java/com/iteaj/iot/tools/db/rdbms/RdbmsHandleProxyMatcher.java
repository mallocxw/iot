package com.iteaj.iot.tools.db.rdbms;

import com.iteaj.iot.handle.proxy.ProtocolHandleProxy;
import com.iteaj.iot.tools.db.DBManager;
import com.iteaj.iot.tools.db.DBProtocolHandleProxyMatcher;

public class RdbmsHandleProxyMatcher implements DBProtocolHandleProxyMatcher {

    private final RdbmsSqlManager rdbmsSqlManager;

    public RdbmsHandleProxyMatcher(RdbmsSqlManager rdbmsSqlManager) {
        this.rdbmsSqlManager = rdbmsSqlManager;
    }

    @Override
    public boolean matcher(Object target) {
        return target instanceof RdbmsHandle;
    }

    @Override
    public DBManager getDbManager() {
        return rdbmsSqlManager;
    }

    @Override
    public Class<? extends ProtocolHandleProxy> getProxyClass() {
        return RdbmsHandle.class;
    }
}

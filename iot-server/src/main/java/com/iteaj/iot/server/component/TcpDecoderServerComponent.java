package com.iteaj.iot.server.component;

import com.iteaj.iot.*;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.IotSocketServer;
import com.iteaj.iot.server.ServerMessage;
import com.iteaj.iot.server.TcpServerComponent;
import io.netty.channel.ChannelPipeline;
import org.springframework.beans.BeanUtils;
import org.springframework.core.GenericTypeResolver;

import java.lang.reflect.Constructor;

/**
 * create time: 2021/2/20
 *
 * @author iteaj
 * @since 1.0
 */
public abstract class TcpDecoderServerComponent<M extends ServerMessage> extends TcpServerComponent<M> implements IotProtocolFactory<M> {

    private ProtocolFactoryDelegation delegation;

    public TcpDecoderServerComponent(ConnectProperties connectProperties) {
        super(connectProperties);
        this.delegation = new ProtocolFactoryDelegation(this, protocolTimeoutStorage());
    }

    @Override
    public abstract String getName();

    @Override
    protected IotProtocolFactory createProtocolFactory() {
        return this;
    }

    @Override
    public void init(Object... args) {
        this.doInitChannel((ChannelPipeline) args[0]);
    }

    @Override
    public AbstractProtocol get(String key) {
        return this.delegation.get(key);
    }

    @Override
    public AbstractProtocol add(String key, Protocol val) {
        return this.delegation.add(key, val);
    }

    @Override
    public AbstractProtocol add(String key, Protocol protocol, long timeout) {
        return this.delegation.add(key, protocol, timeout);
    }

    @Override
    public AbstractProtocol remove(String key) {
        return this.delegation.remove(key);
    }

    @Override
    public boolean isExists(String key) {
        return this.delegation.isExists(key);
    }

    @Override
    public Object getStorage() {
        return this.delegation.getStorage();
    }

}

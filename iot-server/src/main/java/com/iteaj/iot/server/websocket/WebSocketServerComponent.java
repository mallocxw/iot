package com.iteaj.iot.server.websocket;

import com.iteaj.iot.websocket.WebSocketComponent;
import com.iteaj.iot.websocket.WebSocketServerMessage;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.websocketx.WebSocketServerHandshaker;

public interface WebSocketServerComponent<M extends WebSocketServerMessage> extends WebSocketComponent<M> {

    WebSocketServerHandshaker createServerHandShaker(ChannelHandlerContext ctx, HttpRequest req);
}

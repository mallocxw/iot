package com.iteaj.iot.test.filter;

import com.iteaj.iot.CoreConst;
import com.iteaj.iot.Message;
import com.iteaj.iot.codec.filter.RegisterParams;
import com.iteaj.iot.server.websocket.impl.DefaultWebSocketServerComponent;
import com.iteaj.iot.websocket.HttpRequestWrapper;
import com.iteaj.iot.websocket.WebSocketFilter;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.HttpResponseStatus;
import org.springframework.stereotype.Component;

@Component
public class TestWebSocketFilter implements WebSocketFilter<DefaultWebSocketServerComponent> {

    @Override
    public boolean isActivation(Channel channel, DefaultWebSocketServerComponent component) {
        return WebSocketFilter.super.isActivation(channel, component);
    }

    @Override
    public Message.MessageHead register(Message.MessageHead head, RegisterParams params) {
        HttpRequestWrapper httpRequestWrapper = params.getValue(CoreConst.WEBSOCKET_REQ);
        httpRequestWrapper.getQueryParam("client").ifPresent(deviceSn -> head.setEquipCode(deviceSn));

        return head;
    }

    @Override
    public HttpResponseStatus authentication(HttpRequest request) {
        return HttpResponseStatus.OK;
    }
}

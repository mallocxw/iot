package com.iteaj.iot.taos;

import org.springframework.jdbc.core.SqlParameterValue;

import java.util.List;

public class EntitySql {

    private Object entity;

    private String tableName;

    private List<SqlParameterValue> values;

    public EntitySql(String tableName, Object entity, List<SqlParameterValue> values) {
        this.values = values;
        this.entity = entity;
        this.tableName = tableName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<SqlParameterValue> getValues() {
        return values;
    }

    public void setValues(List<SqlParameterValue> values) {
        this.values = values;
    }
}

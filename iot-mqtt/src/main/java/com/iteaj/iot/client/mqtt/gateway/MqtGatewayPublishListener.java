package com.iteaj.iot.client.mqtt.gateway;

import com.iteaj.iot.client.IotClientBootstrap;
import com.iteaj.iot.client.mqtt.MessageMapper;
import com.iteaj.iot.client.mqtt.MessagePublishListener;
import com.iteaj.iot.client.mqtt.MqttClient;
import com.iteaj.iot.consts.ExecStatus;

public class MqtGatewayPublishListener implements MessagePublishListener {

    @Override
    public void success(MqttClient client, MessageMapper mapper) {
        String messageId = mapper.getMessage().getHead().getMessageId();
        MqttGatewayProtocol protocol = (MqttGatewayProtocol)client.getClientComponent().remove(messageId);
        if(protocol != null) {
            protocol.setExecStatus(ExecStatus.success);
            protocol.exec(IotClientBootstrap.businessFactory);
        }
    }
}
